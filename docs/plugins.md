# NPM Packets

## Express

npm-url: https://www.npmjs.com/package/express
docs: https://expressjs.com/
short-description: Fast, unopinionated, minimalist web framework for node.## Mongoose

## Express Middleware - Body-Parser

npm-url: https://www.npmjs.com/package/body-parser
docs: https://www.npmjs.com/package/body-parser
short-description: Parse incoming request bodies in a middleware before your
handlers, available under the req.body property.

## jsonwebtoken
npm-url: https://www.npmjs.com/package/jsonwebtoken
docs: https://www.npmjs.com/package/jsonwebtoken
short-description: An implementation of JSON Web Tokens

## bcryptjs
npm-url: https://www.npmjs.com/package/bcryptjs
docs: https://www.npmjs.com/package/bcryptjs
short-description: Lib to help you hash passwords

## MongoDb
npm-url: https://www.npmjs.com/package/mongodb
docs: https://mongodb.github.io/node-mongodb-native/
short-description: Elegant MongoDB object modeling for Node.js

## Mongoose

npm-url: https://www.npmjs.com/package/mongoose
docs: http://mongoosejs.com/docs/guide.html
short-description: Provides a high-level API on top of mongodb-core that is
meant for end users.

## Lodash

npm-url: https://www.npmjs.com/package/lodash
docs: https://lodash.com/docs/4.17.5
short-description: A modern JavaScript utility library delivering modularity,
performance & extras.

## Mocha

npm-url: https://www.npmjs.com/package/mocha
docs: https://mochajs.org/
short-description: Mocha is a feature-rich JavaScript test framework running on
Node.js and in the browser, making asynchronous testing simple and fun. Mocha
tests run serially, allowing for flexible and accurate reporting, while mapping
uncaught exceptions to the correct test cases.

## Expect

npm-url: https://www.npmjs.com/package/expect
docs: https://facebook.github.io/jest/docs/en/snapshot-testing.html
short-description: Delightful JavaScript Testing

## Supertest

npm-url: https://www.npmjs.com/package/supertest
docs: https://www.npmjs.com/package/supertest
short-description: The motivation with this module is to provide a high-level
abstraction for testing HTTP, while still allowing you to drop down to the
lower-level API provided by superagent.


