# User

## email

## password

## createdAt
## firstLoginAt
## lastLoginAt

## tokens

# Nomad

## name
## address
## birthday
## nationality
## reviews
## languages spoken
## rate
## pictures
## about
## skills
This field will be used to help locals or hosts to accept *
## interests
This filed will be used to filter job/experience opportunities
## type (Nomad/Local)
If * is traveling * is a Nomad else, a Local

# Host

## name
## address
## about
## reviews
## rate
## pictures
## job to offer

# Job

## responsible
People in charge to receive this Nomad
## skills needed
## have to offer
## languages
## schedule

