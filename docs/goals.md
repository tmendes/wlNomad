# Goals

* Build something to help travelers/nomads
* The communicaton between the traveler and the host must be cristal clear since
    the search periody
* Make it simple and safe to exchange experience in between travelers and locals

# Every Traveler should

* have a profile
* be able to define the places he/she is going to be visiting and the dates
    when they will be passing trough each city
* be able to tell the system what kind of skills they have
* be able to tell the system what kind of skills they want to learn from the
    experience.
* be able to tell about their language skills
* be able to tell about their food restrictions
* be able to tell what they are looking for from this experience
* be able to see a list of the oportunities they have on every city they will be
    passing through
* send a message to every host
* be able to find other travelers based on filters
* be able to write a review about their last host
* be able to read others travelers reviews
* be able to see a map showing the places where hosts are located
* be able to see the host profile
* be able to create a group of travelers to apply for an experience

# Every host should

* have a profile
* review an experience with a traveler or a group
* set a description from his/her place
* set info about the people who are going to receive and look forward the
    traveler or group who is coming
* set a new exchange experience
* set a description
* set the type of traveling the host is looking for to have
* set what kind of facilities they are offering
* set what kind fo experiences they are offering

# Possibilities

## Host

Person who are willing to exchange one or more tasks for a bit of help from the
traveller. It could be a traveller helping a host setting up a hostel computer
system in exchange for food, accomodation and a social envirionment

## Local

Person who are willing to exchange experiences with a traveler. It could be a
local who wants pratice a foreign language in exchange for an accomodation.

## Traveler

Person who is traveling and looking for some new experiences. This experience
can be meeting a local for cultural exchange or a host to save money during the
trip.

# Keep in mind

There are contries where internet speed is very slow and since we are dealing
with people who are traveling the world there is a huge chance of nomads who are
using the system being in such place. So we must build a system of choices
where everyone has the chance to choose what kind of layout/features should show
up. The internet we have today are based on the concept that we have good
internet speed and good hardware at the client site so we build it try to use
that in our favor to create a better experience to the user. Our case is
different. We must thing the best way of giving a nomad a way of choosing having
a full feature/javascript/css website and a very simple one that just do
whatever it needs to be done.
