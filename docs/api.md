## POST /nomad/signup

### Short description

Create a new Nomad

### Waiting for

* html_header: normal
* html_body  : { name, email, password }

### Returns

#### Success

* html_header: normal
* html_body  : instructions to the nomad to go to check his/her e-mail for a
code to confirm the account.

#### Fail

* html_header: normal
* html_body  : { err }


## POST /nomad/verify

After receive an email with a verify token the nomad has to go to visit this
link

### Waiting for

* html_header: normal
* html_body  : {email, token}

### Returns

#### Success

* Takes the user to the login page

#### Fail

* html_header: normal
* html_body  : { err }

## POST /nomad/login

Login into the system

### Waiting for

* html_header: normal
* html_body  : {email, password}

### Returns

#### Success

* Takes the user to /nomad

Return:
* html_hader: x-auth:token
* html_body : normal

#### Fail

* html_header: normal
* html_body  : { err }
