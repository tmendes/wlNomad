# Resumo

(Faça um resumo da nova feature)

# Descrição detalhada

(Faça uma descrição detalhada desta feature)

# Quem deve ter acesso a essa feature?

(Descrever qual o nível de acesso desta feature: usuário, todos, admin, etc)

# Diagramas relevantes

(Cole aqui os diagramas que acredite ser relevante para entender o funcionamento
dessa feature)
