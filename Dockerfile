# Pick a base image
FROM node

MAINTAINER thiago _posteo de

# Create the structure to run our system
RUN mkdir -p /app

# Define the working directory
WORKDIR /app

# Copy and cache npm dependences
COPY package.json /app
COPY package-lock.json /app
COPY src/ /app/src

# Env variables
ENV NODE_ENV docker-test

# Run npm install
RUN npm --quiet install

ENTRYPOINT ["npm", "run"]

CMD ["test"]
