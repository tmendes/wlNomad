'use strict';

const _ = require('lodash');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

const {log} = require('./../utils/log');

/* --------------------------------------------------------------------SCHEMA */
let nomadSchema = new Schema({
    email: {
        type: String,
        required: true,
        trim: true,
        minlength: 6,
        unique: true
    },
    name: {
        type: String,
        required: true,
        minlength: 6,
        trim: true
    },
    password: {
        type: String,
        required: true,
        minlength: 6
    },
    createdAt: {
        type: Number,
        required: false
    },
    firstLoginAt: {
        type: Number,
        required: false
    },
    lastLoginAt: {
        type: Number,
        required: false
    },
    tokens: [{
        access: {
            type: String,
            required: false
        },
        token: {
            type: String,
            required: false
        }
    }]
});

/* ---------------------------------------------------------------------UTILS */
/**
 * Generates a new Token
 * @param {string} access - the type of access 'auth', 'emailchecker'
 * @param {string} _id - nomad id
 * @param {string} expires - token will expire in
 * @return {strind} token - the token
 */
let genToken = (access, _id, expires) => {
    if (expires) {
        return jwt.sign({data: {_id, access}}, process.env.JWT_SECRET,
            {expiresIn: expires}).toString();
    } else {
        return jwt.sign({data: {_id, access}}, process.env.JWT_SECRET)
            .toString();
    }
};

/* ----------------------------------------------------------INSTANCE METHODS */
/**
 * Transform something into a JSON object
 * @return {ObjectId} {{ObjectId} _id, {string} name, {string} email}
 */
nomadSchema.methods.toJSON = function() {
    const self = this.toObject();
    return _.pick(self, ['_id', 'email', 'name']);
};

/**
 * Create a new Auth Token to be used by the nomad to login into the system
 * @return {string} token
*/
nomadSchema.methods.genAuthToken = function() {
    const self = this;
    const access = 'auth';
    let token = genToken(access, self._id.toHexString(),
        process.env.LOGIN_TIMEOUT);
    self.tokens.push({access, token});
    return token;
};

/**
 * Create a new Email Checker Token to be used by the nomad to verify her/his
 * account
 * @return {string} token
*/
nomadSchema.methods.genEmailCheckerToken = function() {
    const self = this;
    const access = 'emailchecker';
    const token = genToken(access, self._id.toHexString());
    self.tokens.push({access, token});
    return token;
};

/**
 * Remove a token from the nomad document
 * @param {JsonWebToken} tokenToRemove
 * @return {Promise} Promise - {Nomad} resolve {null} reject
 */
nomadSchema.methods.removeToken = function(tokenToRemove) {
    const self = this;
    return self.update({
        $pull: {tokens: {token: tokenToRemove}}}).then(() => {
        return self;
    }).catch(() => {
        log.err('Unable to remove the tokem from the nomad');
        return null;
    });
};

/* ------------------------------------------------------------STATIC METHODS */
/**
 * Find a Nomad using a Token
 * @param {JsonWebToken} token
 * @return {Promise} Promise - {Nomad} resolve {Error} reject
 */
nomadSchema.statics.findByToken = function(token) {
    let decoded = null;
    try {
        decoded = jwt.verify(token, process.env.JWT_SECRET);
    } catch (e) {
        return Promise.reject(e);
    }

    return Nomad.find({
        '_id': decoded.data._id,
        'tokens.token': token,
        'tokens.access': decoded.data.access
    });
};

/**
 * Find a Nomad using email and password
 * @param {string} email
 * @param {string} password
 * @return {Promise} Promise - {Nomad} resolve {{errormsg {string}} reject
 */
nomadSchema.statics.findByCredentials = function(email, password) {
    return Nomad.findOne({email}).then(nomad => {
        if (!nomad) {
            return Promise.reject({errmsg: 'Nomad does not exists'});
        } else {
            return bcrypt.compare(password, nomad.password).then(result => {
                if (result) {
                    return nomad;
                } else {
                    reject({emmmsg: 'Incorrect Password'});
                }
            });
        }
    });
};

/* ----------------------------------------------------------PRE/POST SCRIPTS */
/**
 * Update the nomad document before save it to the collection
 */
nomadSchema.pre('save', function(next) {
    // eslint-disable-next-line
    const self = this;
    if (!self.createdAt) {
        self.genEmailCheckerToken();
        self.createdAt = new Date().getTime();
    }

    if (self.isModified('password')) {
        bcrypt.genSalt(process.env.SALT_TURNS).then(salt => {
            bcrypt.hash(self.password, salt).then((hash) => {
                self.password = hash;
                next();
            }).catch(e => {
                // FIXME Should not create a nomad
                log.error('Unable to hash the nomad password');
                next();
            });
        }).catch(e => {
            // FIXME Should not create a nomad
            log.error('Unable to hash the nomad password');
            next();
        });
    } else {
        next();
    }
});

/* ------------------MODEL BUILD / MUST BE AFTER THE MOETHODS TO COMPILE THEM */
/* Compilig the nomad Schema into a Model */
const Nomad = mongoose.model('Nomad', nomadSchema);

/* --------------------------------------------------------------------EXPORT */
module.exports = {Nomad};
