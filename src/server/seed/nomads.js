'use strict';

const {ObjectID} = require('mongodb');
const {Nomad} = require('./../models/nomad');
const {log} = require('./../utils/log');

const nomadAId = new ObjectID();
const nomadBId = new ObjectID();
const nomadCId = new ObjectID();
const nomadDId = new ObjectID();
const nomadEId = new ObjectID();
const nomadFId = new ObjectID();

/* -----------------------------------------------------------------SEED DATA */
const nomadsSeed = [
    {
        _id: nomadAId,
        name: 'Kira Salak',
        email: 'kirasalakA@wlnomad',
        password: 'chicago1971'
    },
    {
        _id: nomadBId,
        name: 'Charles Darwin',
        email: 'charlesdarvin@wlnomad',
        password: 'england1882'

    },
    {
        _id: nomadCId,
        name: 'Ernest Shackleton',
        email: 'ernestshackleton@wlnomad',
        password: 'ireland1874'
    }
];

const nomadsExtraSeed = [
    {
        _id: nomadDId,
        name: 'Grace O’ Malley',
        email: 'gracemalley@wlnomad',
        password: 'ireland1530'
    },
    {
        _id: nomadEId,
        name: 'Amelia Earhart',
        email: 'ameliaearhart@wlnomad',
        password: 'kansas1897'
    },
    {
        _id: nomadFId,
        name: 'Annie Londonderry',
        email: 'annielondonderry@wlnomad',
        password: 'latvia1870'
    }
];

/* --------------------------------------------------------------SEED HELPERS */
const populateNomads = (done) => {
    Nomad.remove({}).then(() => {
        log.info(`Adding ${nomadsSeed.length} nomads to the nomad db`);
        let nomadsToSave = nomadsSeed.map((nomad) => {
            return new Nomad(nomad).save();
        });
        return Promise.all(nomadsToSave);
    }).then(() => done()).catch(() => {
        log.err('Unable to add nomads to the nomad db');
    });
};

/* Returns a nomad that should already exists at the Nomad collection */
const getANomad = () => {
    return nomadsSeed[Math.floor(Math.random() * nomadsSeed.length)];
};

/* Returns a new nomad to save at the Nomad collection */
const getAnExtraNomad = () => {
    return nomadsExtraSeed[Math.floor(Math.random() * nomadsExtraSeed.length)];
};

/* --------------------------------------------------------------------EXPORT */
module.exports = {nomadsSeed, nomadsExtraSeed, getANomad, getAnExtraNomad,
    populateNomads};
