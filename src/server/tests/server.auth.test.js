'use strict';

const request = require('supertest');

const {app} = require('./../server');
const {Nomad} = require('./../models/nomad');
const {getANomad} = require('./../seed/nomads');

describe('Server Auth tests', () => {
    describe('GETs', () => {
        describe('/nomad', () => {
            it('should success if I have a valid token', (done) => {
                const nomadGodInfo = getANomad();
                Nomad.findOne({email: nomadGodInfo.email}).then(nomad => {
                    const token = nomad.genAuthToken();
                    request(app).
                        get('/nomad').
                        set('x-auth', token).
                        expect(200).
                        end(done);
                }).catch(e => done(e));
            });

            it('should fail if I have an invalid token', (done) => {
                const nomadGodInfo = getANomad();
                Nomad.findOne({email: nomadGodInfo.email}).then(nomad => {
                    request(app).
                        get('/nomad').
                        set('x-auth', 'invalid.token').
                        expect(401).
                        end(done);
                }).catch(e => done(e));
            });
        });
    });
});
