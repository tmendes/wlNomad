'use strict';

const expect = require('expect');
const {nomadsSeed, nomadsExtraSeed,
    getANomad, getAnExtraNomad} = require('./../seed/nomads');

describe('Nomad Seeds', () => {
    it('shoud return some nomad info', () => {
        const nomad = getANomad();
        expect(nomad).toHaveProperty('name');
        expect(nomad).toHaveProperty('_id');
        expect(nomad).toHaveProperty('password');
        expect(nomad).toHaveProperty('email');
    });

    it('shoud return some nomad info (extra nomads)', () => {
        const nomad = getAnExtraNomad();
        expect(nomad).toHaveProperty('name');
        expect(nomad).toHaveProperty('_id');
        expect(nomad).toHaveProperty('password');
        expect(nomad).toHaveProperty('email');
    });

    it('should have nomads at the seed array', () => {
        expect(nomadsSeed.length).toBeGreaterThan(0);
        nomadsSeed.forEach(nomad => {
            expect(nomad).toHaveProperty('name');
            expect(nomad).toHaveProperty('_id');
            expect(nomad).toHaveProperty('password');
            expect(nomad).toHaveProperty('email');
        });
    });

    it('should have extra nomads at the seed array', () => {
        expect(nomadsExtraSeed.length).toBeGreaterThan(0);
        nomadsExtraSeed.forEach(nomad => {
            expect(nomad).toHaveProperty('name');
            expect(nomad).toHaveProperty('_id');
            expect(nomad).toHaveProperty('password');
            expect(nomad).toHaveProperty('email');
        });
    });
});
