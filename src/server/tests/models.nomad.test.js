'use strict';

const expect = require('expect');
const bcrypt = require('bcryptjs');

const {Nomad} = require('./../models/nomad');
const {populateNomads, getANomad,
    getAnExtraNomad} = require('./../seed/nomads');

beforeEach(populateNomads);

describe('Nomad Model', () => {
    describe('Basic Methods', () => {
        it('should return a JSON object', () => {
            const newNomadGodInfo = getAnExtraNomad();
            let newNomadGod = new Nomad(newNomadGodInfo);

            newNomadGod.awesomeness = 'Unbelievable';
            newNomadGod.advScore = '100';

            const nomadJSON = newNomadGod.toJSON();

            expect(nomadJSON).toHaveProperty('_id');
            expect(nomadJSON).toHaveProperty('email');

            expect(nomadJSON).not.toHaveProperty('awesomeness');
            expect(nomadJSON).not.toHaveProperty('advScore');
        });

        it('should add a created timestamp to a new nomad', done => {
            const newNomadGodInfo = getAnExtraNomad();
            const newNomadGod = new Nomad(newNomadGodInfo);

            newNomadGod.save().then(nomad => {
                expect(typeof nomad.createdAt).toBe('number');
                done();
            }).catch(e => done(e));
        });
    });

    describe('Tokens', () => {
        describe('Auth', () => {
            it('should generate a Auth Token for a Nomad', () => {
                const nomadGodInfo = getAnExtraNomad();
                const newNomadGod = new Nomad(nomadGodInfo);
                const token = newNomadGod.genAuthToken();
                const len = newNomadGod.tokens.length;
                expect(len).toBeGreaterThan(0);
                expect(newNomadGod.tokens[len-1].token).not.toBeUndefined();
                expect(newNomadGod.tokens[len-1].token).toBe(token);
                expect(newNomadGod.tokens[len-1].access).toBe('auth');
            });

            //        it(`should expire the Auth Token after 24h`,
            //            () => {
            //                const nomadGodInfo = getAnExtraNomad();
            //                const newNomadGod = new Nomad(nomadGodInfo);
            //                const token = newNomadGod.genAuthToken();
            //                //TODO How to test it? :O
            //            });
        });

        describe('Email Checker', () => {
            it('should generate a Email checker Token from a Nomad', () => {
                const nomadGodInfo = getAnExtraNomad();
                const newNomadGod = new Nomad(nomadGodInfo);

                const token = newNomadGod.genEmailCheckerToken();
                const len = newNomadGod.tokens.length;
                expect(len).toBeGreaterThan(0);
                expect(newNomadGod.tokens[len-1].token).not.toBeUndefined();
                expect(newNomadGod.tokens[len-1].token).toBe(token);
                expect(newNomadGod.tokens[len-1].access).toBe('emailchecker');
            });
        });

        describe('Utils', () => {
            it('should return a nomad using a valid token', done => {
                const newNomadGodInfo = getAnExtraNomad();
                const newNomadGod = new Nomad(newNomadGodInfo);
                const token = newNomadGod.genAuthToken();
                newNomadGod.save().then(() => {
                    return Nomad.findByToken(token);
                }).then(nomad => {
                    expect(nomad[0].email).toBe(newNomadGodInfo.email);
                    done();
                }).catch(e => done(e));
            });

            it('should not return a nomad using an invalid token', done => {
                Nomad.findByToken('invalid.token').catch(e => {
                    expect(e).not.toBeUndefined();
                    done();
                });
            });
        });
    });

    describe('Password', () => {
        it('should have a hash for password', done => {
            const nomadGodInfo = getANomad();
            Nomad.findOne({email: nomadGodInfo.email}).then(nomad => {
                expect(nomad.password).not.toBe(nomadGodInfo.password);
                return bcrypt.compare(nomadGodInfo.password, nomad.password);
            }).then(result => {
                expect(result).toBeTruthy();
                done();
            }).catch(e => done(e));
        });

        it('should success for a valid password', done => {
            const nomadGodInfo = getANomad();
            Nomad.findOne({email: nomadGodInfo.email}).then(nomad => {
                return bcrypt.compare(nomadGodInfo.password, nomad.password);
            }).then(result => {
                expect(result).toBeTruthy();
                done();
            }).catch(e => done(e));
        });

        it('should fail for an invalid password', done => {
            const nomadGodInfo = getANomad();
            Nomad.findOne({email: nomadGodInfo.email}).then(nomad => {
                return bcrypt.compare('invalid.password', nomad.password);
            }).then(result => {
                expect(result).not.toBeTruthy();
                done();
            }).catch(e => done(e));
        });
    });

    describe('Authenticate', () => {
        it('should success - correct email and password', done => {
            const nomadGodInfo = getANomad();
            Nomad.findByCredentials(nomadGodInfo.email, nomadGodInfo.password).
                then(nomad => {
                    expect(nomad.email).toBe(nomadGodInfo.email);
                    done();
                }).catch(e => done(e));
        });

        it('should fail - undefined email', done => {
            const nomadGodInfo = getANomad();
            Nomad.findByCredentials(undefined, nomadGodInfo.password).
                catch(e => {
                    expect(e).not.toBeUndefined();
                    done();
                });
        });


        it('should fail - incorrect email', done => {
            const nomadGodInfo = getANomad();
            Nomad.findByCredentials('incorrect.email', nomadGodInfo.password).
                catch(e => {
                    expect(e).not.toBeUndefined();
                    done();
                });
        });

        it('should fail - undefined password', done => {
            Nomad.findByCredentials('incorrect.email', undefined).
                catch(e => {
                    expect(e).not.toBeUndefined();
                    done();
                });
        });

        it('should fail - incorrect password', done => {
            const nomadGodInfo = getANomad();
            Nomad.findByCredentials(nomadGodInfo.email, 'incorrect.password').
                catch(e => {
                    expect(e).not.toBeUndefined();
                    done();
                });
        });
    });

    // FIXME Check what is going on here
    // it('should remove a Token from a nomad', done => {
    //     const nomadGodInfo = getANomad();
    //     const nomadGod = new Nomad(nomadGodInfo);
    //     let len;

    //     nomadGod.genAuthToken().then((tokenKey) => {
    //         len = nomadGod.tokens.length;
    //         expect(len).toBeGreaterThan(0);
    //         expect(nomadGod.tokens[len-1].token).not.toBeUndefined();
    //         expect(nomadGod.tokens[len-1].token).toBe(tokenKey);
    //         return nomadGod.removeToken(tokenKey);
    //     }).then((nomad) => {
    //             const newLen = nomad.tokens.length;
    //             expect(newLen).toBe(len - 1);
    //             done();
    //     }).catch(e => done(e));
    // });
});
