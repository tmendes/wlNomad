'use strict';

const expect = require('expect');
const request = require('supertest');

const {app} = require('./../server');
const {Nomad} = require('./../models/nomad');
const {populateNomads, getANomad, getAnExtraNomad} =
    require('./../seed/nomads');

/* ----------------------------------------------------- TEST PRE/POS SCRIPTS */
/* Remove/Add nomads to the database before each test */
beforeEach(populateNomads);

describe('Server API', () => {
    describe('Signup', () => {
        describe('/nomad/signup', () => {
            it('should create a new nomad', (done) => {
                let newNomadGodInfo = getAnExtraNomad();
                delete newNomadGodInfo._id;

                request(app).
                    post('/nomad/signup').
                    send(newNomadGodInfo).
                    expect(200).
                    end((err, res) => {
                        if (err) {
                            return done(err);
                        }

                        Nomad.findOne({email: newNomadGodInfo.email}).then(
                            nomad => {
                                expect(nomad).not.toBeNull();
                                done();
                            }).catch(e => done(e));
                    });
            });

            it('should not create a new nomad - email is already in use',
                (done) => {
                    const nomadGodInfo = getANomad();
                    let numberOfNomadsCounter;

                    Nomad.count({}, (err, cnt) => {
                        numberOfNomadsCounter = cnt;
                    }).catch(e => done(e));

                    request(app).
                        post('/nomad/signup').
                        send(nomadGodInfo).
                        expect(400).
                        end((err, res) => {
                            if (err) {
                                return done(err);
                            }
                            Nomad.count({}, (error, cnt) => {
                                expect(numberOfNomadsCounter).toBe(cnt);
                                done();
                            }).catch(e => done(e));
                        });
                });

            it('should not create a new nomad - missing password', done => {
                let newNomadGodInfo = getAnExtraNomad();
                delete newNomadGodInfo.password;

                request(app).
                    post('/nomad/signup').
                    send(newNomadGodInfo).
                    expect(400).
                    end((err, res) => {
                        if (err) {
                            return done(err);
                        }

                        Nomad.findOne({email: newNomadGodInfo.email}).then(
                            nomad => {
                                expect(nomad).toBeNull();
                                done();
                            }).catch(e => done(e));
                    });
            });

            it('should not create a new nomad - missing name', done => {
                let newNomadGodInfo = getAnExtraNomad();
                delete newNomadGodInfo.name;

                request(app).
                    post('/nomad/signup').
                    send(newNomadGodInfo).
                    expect(400).
                    end((err, res) => {
                        if (err) {
                            return done(err);
                        }

                        Nomad.findOne({email: newNomadGodInfo.email}).then(
                            nomad => {
                                expect(nomad).toBeNull();
                                done();
                            }).catch(e => done(e));
                    });
            });

            it('should not create a new nomad - missing email', done => {
                let newNomadGodInfo = getAnExtraNomad();
                delete newNomadGodInfo.email;

                request(app).
                    post('/nomad/signup').
                    send(newNomadGodInfo).
                    expect(400).
                    end((err, res) => {
                        if (err) {
                            return done(err);
                        }

                        Nomad.findOne({name: newNomadGodInfo.name}).then(
                            nomad => {
                                expect(nomad).toBeNull();
                                done();
                            }).catch(e => done(e));
                    });
            });

            it('should not create a new nomad - password too short', done => {
                let newNomadGodInfo = getAnExtraNomad();
                newNomadGodInfo.password = '1234';

                request(app).
                    post('/nomad/signup').
                    send(newNomadGodInfo).
                    expect(400).
                    end((err, res) => {
                        if (err) {
                            return done(err);
                        }

                        Nomad.findOne({email: newNomadGodInfo.email}).then(
                            nomad => {
                                expect(nomad).toBeNull();
                                done();
                            }).catch(e => done(e));
                    });
            });

            it('should not create a new nomad - name too short', done => {
                let newNomadGodInfo = getAnExtraNomad();
                newNomadGodInfo.name = 'me';

                request(app).
                    post('/nomad/signup').
                    send(newNomadGodInfo).
                    expect(400).
                    end((err, res) => {
                        if (err) {
                            return done(err);
                        }

                        Nomad.findOne({email: newNomadGodInfo.email}).then(
                            nomad => {
                                expect(nomad).toBeNull();
                                done();
                            }).catch(e => done(e));
                    });
            });

            it('should not create a new nomad - email too short', done => {
                let newNomadGodInfo = getAnExtraNomad();
                newNomadGodInfo.email = 'm@de';

                request(app).
                    post('/nomad/signup').
                    send(newNomadGodInfo).
                    expect(400).
                    end((err, res) => {
                        if (err) {
                            return done(err);
                        }

                        Nomad.findOne({email: newNomadGodInfo.email}).then(
                            nomad => {
                                expect(nomad).toBeNull();
                                done();
                            }).catch(e => done(e));
                    });
            });

            it('It should fail if I send invalid type of data to the server',
                () => {
                    request(app).
                        post('/nomad/signup').
                        send('test').
                        expect(400).
                        // TODO Expect for something from the server
                        end();
                }
            );

            it('It should fail if I send no data to the server',
                () => {
                    request(app).
                        post('/nomad/signup').
                        send({}).
                        expect(400).
                        // TODO Expect for something from the server
                        end();
                }
            );
        });
    });

    describe('Verify Email After Signup', () => {
        describe('/nomad/verify', () => {
            it('It should fail if I send invalid type of data to the server',
                () => {
                    request(app).
                        post('/nomad/verify').
                        send('test').
                        expect(400).
                        // TODO Expect for something from the server
                        end();
                });

            it('It should fail if I send no data to the server',
                () => {
                    request(app).
                        post('/nomad/verify').
                        send({}).
                        expect(400).
                        // TODO Expect for something from the server
                        end();
                });

            it('It should fail if I send a invalid token', () => {
                const nomadGodInfo = getANomad();

                const data = {email: nomadGodInfo.email, token: 'faketoken'};
                request(app).
                    post('/nomad/verify').
                    send(data).
                    expect(400).
                    // TODO Expect for something from the server
                    end();
            });

            it('It should fail if I do not send a token', () => {
                const nomadGodInfo = getANomad();

                const data = {email: nomadGodInfo.email};
                request(app).
                    post('/nomad/verify').
                    send(data).
                    expect(400).
                    // TODO Expect for something from the server
                    end();
            });

            it('should fail if it has an invalid or undefined email', () => {
                const data = {email: 'businessman@bigcity', token: ''};
                request(app).
                    post('/nomad/verify').
                    send(data).
                    expect(400).
                    // TODO Expect for something from the server
                    end();
            });

            it('should return 200 if nomad exists and has a valid token',
                done => {
                    const nomadGodInfo = getANomad();
                    Nomad.findOne({email: nomadGodInfo.email}).then(nomad => {
                        const token = nomad.genEmailCheckerToken();
                        const data = {
                            email: nomadGodInfo.email,
                            token: token
                        };
                        request(app).
                            post('/nomad/verify').
                            send(data).
                            expect(200).
                            // TODO Expect for something from the server
                            end(done);
                    }).catch(e => done(e));
                });
        });
    });

    describe('Nomad Login', () => {
        describe('/nomad/login', () => {
            it('should success - valid email and password', done => {
                const nomadGodInfo = getANomad();
                const data = {email: nomadGodInfo.email,
                    password: nomadGodInfo.password};
                request(app).
                    post('/nomad/login').
                    send(data).
                    expect(200).
                    expect(res => {
                        expect(res.headers['x-auth']).toBeTruthy();
                    }).
                    end(done);
            });


            it('should fail - invalid email', done => {
                const nomadGodInfo = getANomad();
                const data = {email: 'invalid.email',
                    password: nomadGodInfo.password};
                request(app).
                    post('/nomad/login').
                    send(data).
                    expect(400).
                    end(done);
            });

            it('should fail - no email', done => {
                const nomadGodInfo = getANomad();
                const data = {password: nomadGodInfo.password};
                request(app).
                    post('/nomad/login').
                    send(data).
                    expect(400).
                    end(done);
            });

            it('should fail - invalid password', done => {
                const nomadGodInfo = getANomad();
                const data = {email: nomadGodInfo.email,
                    password: 'invalid.password'};
                request(app).
                    post('/nomad/login').
                    send(data).
                    expect(400).
                    end(done);
            });

            it('should fail - no password', done => {
                const nomadGodInfo = getANomad();
                const data = {email: nomadGodInfo.email};
                request(app).
                    post('/nomad/login').
                    send(data).
                    expect(400).
                    end(done);
            });

            it('should fail - no data', done => {
                request(app).
                    post('/nomad/login').
                    send({}).
                    expect(400).
                    end(done);
            });

            it('should fail - wrong type of data', done => {
                request(app).
                    post('/nomad/login').
                    send('invalid.data').
                    expect(400).
                    end(done);
            });
        });
    });
});
