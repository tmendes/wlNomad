'use strict';

const {Nomad} = require('./../models/nomad.js');

/**
 * @brief
 * Gets the x-auth token from the html
 * and check if it is a valid token
 * @param {Object} req - request object
 * @param {Object} res - response object
 * @param {Fucntion} next - next middleware function in the stack
 * @return {Object} reject if token is undefined
 */
const authenticate = (req, res, next) => {
    const token = req.header('x-auth');

    if (token === undefined) {
        return Promise.reject();
    }

    Nomad.findByToken(token).then(nomad => {
        if (!nomad) {
            return Promise.reject();
        }
        req.nomad = nomad;
        req.token = token;
        next();
    }).catch( () => {
        res.status(401).send();
    });
};

module.exports = {authenticate};
