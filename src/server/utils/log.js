'use strict';

const env = process.env.NODE_ENV;

let log;

if (env.includes('test')) {
    log = {
        err: function(msg) {
            return;
        },
        suc: function(msg) {
            return;
        },
        info: function(msg) {
            return;
        },
        warn: function(msg) {
            return;
        }
    };
} else {
    log = {
        err: function(msg) {
            console.error(`/o\\ ${msg}`);
        },
        suc: function(msg) {
            console.log(`\\o/ ${msg}`);
        },
        info: function(msg) {
            console.info(`o ${msg}`);
        },
        warn: function(msg) {
            console.warn(`(>_<) ${msg}`);
        }
    };
}

module.exports = {log};
