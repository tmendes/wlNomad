'use strict';

const express = require('express');
const router = express.Router();

const _ = require('lodash');

// eslint-disable-next-line
const mongoose = require('./../db/mongoose');
const {Nomad} = require('./../models/nomad');

const {authenticate} = require('./../midlleware/authenticate');

/* --------------------------------------------------------------------SIGNUP */
/**
 * Create a new nomad into the system
 * @param { {string} name, {string} email, {string} password }
 * @returns Success: { Nomad } Fail: {{string} status, {string} errmsg}
 */
router.post('/signup', async (req, res) => {
    try {
        const newNomadInfo = _.pick(req.body, ['name', 'email', 'password']);
        const newNomad = new Nomad(newNomadInfo);
        const nomad = await newNomad.save();
        res.status(200).send({status: 200, nomad});
    } catch (e) {
        res.status(400).send({status: 400, errmsg: e.errmsg});
    }
});

/**
 * Verify if a new Nomad has a valid email address or not
 * @param {{string} email, {string} token}
 * @returns Fail {{string} status, {string} errmsg}
 */
router.post('/verify', async (req, res) => {
    try {
        const nomadInfo = _.pick(req.body, ['email', 'token']);
        const nomad = await Nomad.findByToken(nomadInfo.token);
        res.status(200).send({status: 200, nomad});
    } catch (e) {
        res.status(400).send({status: 400, errmsg: e.errmsg});
    }
});

/* ---------------------------------------------------------------------LOGIN */
/**
 * Verify the credentials to allow or deny the login
 * @param {{string} email, {string} password}
 * @returns {string} html_header x-auth Token {Nomad} nomad
 */
router.post('/login', async (req, res) => {
    try {
        const nomadInfo = _.pick(req.body, ['email', 'password']);
        const nomad = await
            Nomad.findByCredentials(nomadInfo.email, nomadInfo.password);
        const token = nomad.genAuthToken();
        res.setHeader('x-auth', token);
        res.status(200).send({status: 200, nomad});
    } catch (e) {
        res.status(400).send({status: 400, errmsg: e.errmsg});
    }
});

/* ------------------------------------------------------------------ PROFILE */
router.get('/', authenticate, (req, res) => {
    res.status(200).send({status: 200, nomad: req.nomad});
});

/* ------------------------------------------------------------------- EXPORT */
module.exports = router;
