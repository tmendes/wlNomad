'use strict';

const {log} = require('./../utils/log');

let env = process.env.NODE_ENV || 'development';

log.info(`Server config running at ${env} mode`);

if (env === 'development' || env.includes('test')) {
    const configJSON = require('./server.json');
    const config = configJSON[env];
    const configKeys = Object.keys(config);

    configKeys.forEach((key) => {
        log.info(`Setting ${key} key as ${config[key]}`);
        process.env[key] = config[key];
    });
}

