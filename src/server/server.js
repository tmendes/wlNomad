'use strict';

require('./config/server');
const appConfig = require('./config/app.json');

const express = require('express');
const bodyParser = require('body-parser');

const {log} = require('./utils/log');

const app = express();
const port = process.env.PORT;

const nomadRouters = require('./routes/nomad');

app.use(bodyParser.json());

/* --------------------------------------------------------------------ROUTES */
app.use('/nomad/', nomadRouters);

/* --------------------------------------------------------------------CONFIG */
/* It starts the web server */
app.listen(port, () => {
    log.suc(`${appConfig.name} running at http://localhost:${port}`);
});

/* ------------------------------------------------------------------ EXPORTS */
module.exports = {app};
