# wlNomad Workaway

## The main goal

We want to create a project to give travelers an easy way to exchange the
skills they have for something they need at a specific moment. At the same time
they help people who might need some help. The main idea is to create a
little comunity to help travelers all around the word.

## Why?

We are both travelers and we don't really feel connected with the alternatives
we have today. The experience we have is that most of the times the traveler
will give way more than receive.

## How do we plan to solve this issue?

We want to build something with your help, we are open to new ideas and to push
requests. We believe if we stay connected to the comunity we can build something
to the comunity.

## Why Javascript?

We are both new to the Javascript word. I (Thiago) started to study it a month
ago and we are both very exited with the language itself. Build something
from the screath using Javascript will bring us some experience we don't have
with the language.

## What to expect from this project?

So far it is a lab project. We are building it for the comunity of travelers but
we are doing it at our free time.

## So how can you help us?

If you are no happy with the alternatives we, as travelers, have just send us
a message telling us why are you not happy. Also, if you have any ideas that you
might wanna see just let us know.

## How fast will this project run?

Very slow! As we said before, we are Javascript beginners and we are using our
free time to build it. So, please, for now, don't expect too much from it.

## Thanks to:
<a title="Emoji One [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AEmojione_1F680.svg"><img width="512" alt="Emojione 1F680" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Emojione_1F680.svg/512px-Emojione_1F680.svg.png"/></a>
